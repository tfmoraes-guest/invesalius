#!/usr/bin/make -f
# debian/rules for invesalius
# Andreas Tille <tille@debian.org>
# GPL

export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow
  
package=invesalius

PYMODDIR := /usr/lib/$(shell pyversions -r)
TARGET = $(CURDIR)/debian/$(package)
# shared library versions, option 1
version=2.0.5
major=2

%:
	dh $@ --with python2

override_dh_auto_build: create-cython-dir
	python setup.py build_ext --inplace

override_dh_auto_clean:
	# dh_auto_clean
	rm -f invesalius3

override_dh_compress:
	dh_compress --exclude=LICENSE.pt.txt --exclude=.inv3

create-cython-dir:
	mkdir invesalius_cy/
	touch invesalius_cy/__init__.py
	cp invesalius/data/*.pyx invesalius_cy
	cp invesalius/data/*.pxd invesalius_cy

override_dh_clean:
	dh_clean
	rm -rf invesalius_cy
	rm -rf build

create-launcher:
	echo '#!/bin/sh' > invesalius3
	echo 'export PYTHONPATH=$$PYTHONPATH:"/usr/lib/invesalius"' >> invesalius3
	echo 'export INVESALIUS_LIBRARY_PATH="/usr/share/invesalius/"' >> invesalius3
	echo 'cd $$INVESALIUS_LIBRARY_PATH' >> invesalius3
	echo 'python app.py $$@' >> invesalius3

override_dh_install: create-launcher
	dh_install

override_dh_installchangelogs:
	dh_installchangelogs changelog.md

override_dh_auto_install: ;

override_dh_builddeb:
	dh_builddeb -- -Z xz

get-orig-source:
	mkdir -p ../tarballs
	uscan --verbose --force-download --repack --rename --destdir=../tarballs

.PHONY: override_dh_auto_clean override_dh_install
